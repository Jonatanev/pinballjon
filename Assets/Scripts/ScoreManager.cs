using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    /// <summary>
    /// 	Control del display de scores
    /// </summary>
	public static int score = 0;
	public TextMesh displayScore;
	
   	void Update()
	{
		if(displayScore)
		{
            displayScore.text ="Score: " + score.ToString("D8");
		}
	}
}
