using UnityEngine;
using System.Collections;


public class Flipper : MonoBehaviour
{
    /// <summary>
    /// 	Controla los flippers
    /// </summary>
    public string inputButtonName  = "Fire2";
    public float force = 1000.0f;
    public Vector3 forceDirection = Vector3.forward;
    public Vector3 offset;

  
    // Use this for initialization
    void Start ()
	{
        
    }
    // Update is called once per frame
    void Update()
    {

    }
    
    void FixedUpdate ()
	{
        
        if (Input.GetButton(inputButtonName))
        {
            this.GetComponent<Rigidbody>().AddForceAtPosition(forceDirection.normalized * force, transform.position + offset);
        }
        else
        {
            this.GetComponent<Rigidbody>().AddForceAtPosition(forceDirection.normalized * -force, transform.position + offset);
        }
    }
}
