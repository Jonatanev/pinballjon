﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    /// <summary>
    /// 	Controla por donde va la Ball gameobject en el juego
    /// </summary>

    public GameObject door;
	// Use this for initialization
	void Start () {
        //Desactive Door
        door = GameObject.Find("Door");
        door.SetActive(false);
    }
  

    void OnCollisionEnter(Collision col)
    {
        // When the ball enters a trigger, check if the ball has collided with a drain
        if (col.gameObject.tag == "Drain")
        {
           
            // Destroy this ball
            Destroy(this.gameObject);
            BallManager.lives = BallManager.lives - 1;
        }

       if (col.gameObject.tag == "Flipper")
        {
          
        }
    }
   
    
    
}
