﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityOff : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Ball")
        {
            //Debug.Log("Subeee rampa");
            //col.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            //Physics.gravity = new Vector3(0, -100,0 );
            col.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
            
            //Vector3 forceVec = col.gameObject.GetComponent<Rigidbody>().velocity.normalized * 600;
            //col.gameObject.GetComponent<Rigidbody>().AddForce(forceVec, ForceMode.Acceleration);
            Physics.gravity = new Vector3(0, 0, -10);

        }
    }
}
