﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Repel : MonoBehaviour {

    /// <summary>
    /// 	Scripy que controla los rebotes
    /// </summary>
    /// 
	public float explosionStrength = 100;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider _other)
    {
		if (_other.gameObject.tag == "Ball") {
			//RepelObject(_other.gameObject);
		
		}
	}
    void OnCollisionEnter(Collision _other)
    {
        if (_other.gameObject.tag == "Ball")
        {
            _other.gameObject.GetComponent<Rigidbody>().AddExplosionForce(explosionStrength, transform.position, 5);
        }
    }
    
        void RepelObject(GameObject _other)
    {
        /*Vector3 forceVec = -_other.gameObject.GetComponent<Rigidbody>().velocity.normalized * explosionStrength;
        _other.gameObject.GetComponent<Rigidbody>().AddForce (forceVec, ForceMode.Acceleration);*/
        _other.gameObject.GetComponent<Rigidbody>().AddExplosionForce(explosionStrength, transform.position, 5);
    }
    
	
}
