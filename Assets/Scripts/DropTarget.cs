using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropTarget : MonoBehaviour
{
    /// <summary>
    /// 	Controla los targets
    /// </summary>
    /// 
	public float dropDistance = 1.0f;
	public int bankID = 0;
	public float resetDelay = 0.5f;
	public static List<DropTarget> dropTargets = new List<DropTarget>();
	public int dropValue = 10;
	
	public bool isDropped = false;

	// Use this for initialization
	void Start ()
	{
		dropTargets.Add(this);
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
	
	void OnCollisionEnter()
	{
		if(!isDropped)
		{
			//Bajar el Target
			transform.position += Vector3.down*dropDistance;
			isDropped = true;
			ScoreManager.score += dropValue;
			
			//Reviso si se han bajado todos los targets
			bool resetBank = true;
			foreach(DropTarget target in dropTargets)
			{
				if(target.bankID == bankID)
				{
					if(!target.isDropped)
					{
						resetBank = false;
					}
				}
			}
			
			//Reset los targets si estan todos bajados y multiplico score *3
			if(resetBank)
			{
				ScoreManager.score += ScoreManager.score*3;
				Invoke("ResetBank", resetDelay);
			}
		}
	}
	
	void ResetBank()
	{
		foreach(DropTarget target in dropTargets)
		{
			if(target.bankID == bankID)
			{
				target.transform.position += Vector3.up*dropDistance;
				target.isDropped = false;
			}
		}
	}
}
