﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour {

    /// <summary>
    /// 	Controla la lógica de la door
    /// </summary>
    /// 
    public GameObject door;
    // Use this for initialization
    void Awake () {
        door = GameObject.Find("Door");
        door.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if(BallManager.lives==BallManager.lives-1) //Cuando se pierde una vida, se abre la puerta
        {
             //Desactiva Door
             door.SetActive(false);
            Debug.Log("PUERTAA");
        }

    }
   

    void OnTriggerEnter(Collider _other)
    {
        if (_other.gameObject.tag == "Ball")
        {
            door.SetActive(true);
           
        }
    }
  


}
