using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BallManager : MonoBehaviour
{
    /// <summary>
    /// 	Controla la l�gica de las vidas y balls en el juego
    /// </summary>
    /// 
	public GameObject ball;
    public GameObject coin;
    public Transform spawnPosition;
	public TextMesh display;
	public static int lives;
    public static int balls;
	public  bool gameOver = false;
	
    void Start()
    {
        coin = GameObject.Find("InsertCoin");
        balls = 0;
        lives = 0;
        if (display)
        {
            display.text = "Insert Coin \n or Esc for quit";
            coin.SetActive(true);
        }

    }

	void Update()
	{
		//Check for gameover
		 if(Input.GetKeyUp(KeyCode.Escape) || lives < 0 )
        {
            gameOver = true;
        }
        if(lives==2)
        {
            balls = 3;
        }
      
        if (!GameObject.FindGameObjectWithTag("Ball") && !gameOver && balls>0)
		{
			Instantiate(ball,spawnPosition.position,ball.transform.rotation);
			//Update display
			if(display)
			{
				display.text = "BALLS: " + lives.ToString();
			}
            balls--;
		}
		
	
		if(gameOver)
		{
            display.text = "GAME OVER \n press any Key";
            
            if (Input.anyKeyDown)
			{
                SceneManager.LoadScene(0);
			}
        }
	}
}
