﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour {

    /// <summary>
    /// 	Controla los Bumpers, rebotes y luves
    /// </summary>
    public float explosionStrength  = 500;
    public float forceRadius = 1.0f;
    public int bumperValue = 10;
    public GameObject light;
    private Light lights = new Light();
    bool bFlashing;

    // Use this for initialization
    void Start () {
        lights = light.GetComponentInChildren<Light>();
        light.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision _other)
    {
        if (_other.gameObject.tag == "Ball")
        {
            foreach (Collider col in Physics.OverlapSphere(transform.position, forceRadius))
            {
                if (col.GetComponent<Rigidbody>())
                {
                    col.GetComponent<Rigidbody>().AddExplosionForce(explosionStrength, transform.position, forceRadius);
                }
            }
            ScoreManager.score += bumperValue;
            StartCoroutine(Flash(0.5f));
            
        }

    }

    public IEnumerator Flash(float fWaitTime)
    {
        while (bFlashing)
        {
            yield return new WaitForSeconds(0.1f);
        }
        light.SetActive(true);
        bFlashing = true;
        Debug.Log("FLASHING");
        yield return new WaitForSeconds(fWaitTime);
        light.SetActive(false);
        bFlashing = false;
    }
}
