﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGravity : MonoBehaviour {

    public int direc;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Ball")
        {
            //Debug.Log("Baja rampa");
            col.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            Physics.gravity = new Vector3(0, 0, direc);
        }
    }
}
