﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    /// <summary>
    /// 	Script del Menu, parpadeo imagen, pulsacion tecla
    /// </summary>
    /// 
    Image image;
    float timer = 1f;
    // Use this for initialization
    void Start () {
       
        image = GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;

        if (timer < 0)
        {
            timer = 1f;

            image.enabled = !image.enabled;
        }

        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 
        }
    }
}
