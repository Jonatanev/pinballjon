﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

    /// <summary>
    /// 	Controla el lanzador de bolas
    /// </summary>
    /// 
    string inputButtonName = "Jump";
     float distance = 1.9f;
     float speed = 1.5f;
     Rigidbody ball ;
    float power = 1600;
     bool ready = false;
     bool fire = false;
     float moveCount = 0;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetButton(inputButtonName))
        {
            //Mientras no llega a la distance, la pieza se desplaza
            if (moveCount < distance)
            {
                transform.Translate(0, -speed * Time.deltaTime, 0);
                moveCount += speed * Time.deltaTime;
                fire = true;
            }
        }
        else if (moveCount > 0)
        {
            //Lanza la Ball
            if (fire && ready)
            {
                 ball.transform.TransformDirection(Vector3.forward * 10);
                 ball.AddForce(0, 0, moveCount * power);
                fire = false;
                ready = false;
            }
            //Una vez lanzado, reposicionamos la pieza lanzador
            transform.Translate(0, 20 * Time.deltaTime, 0);
            moveCount -= 20 * Time.deltaTime;
        }

        //Controlo que se resetee hasssta donde se mueve
        if (moveCount <= 0)
        {
            fire = false;
            moveCount = 0;
        }

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Ball")
        {
            Debug.Log("BOLAAA");
            ball = col.GetComponent<Rigidbody>();
            ready = true;
        }
    }
    
}
